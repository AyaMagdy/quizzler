//
//  Question.swift
//  Quizzler
//
//  Created by aya magdy on 2/10/19.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation
class Question {
    let questionText : String
    let answer : Bool
    init(text : String , correctAnswer: Bool) {
     //when create object of this class we call this function
        questionText = text
        answer = correctAnswer
    }
}

//class myOtherClass{
//    let question = Question(text: "what is the meaning of life ?", correctAnswer: true)
//
//}
